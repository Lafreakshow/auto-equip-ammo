Scriptname LAutoEquipKnownAmmoList extends Quest
; This script handles storing known ammo types.

Ammo[] KnownArrows 
Ammo[] KnownBolts

; clear doesn't just clear the arrays, it initialises them, so it hase to run first thing or the entire script
; will operate on uninitialized arrays, which is like really bad.
Event OnInit()
    self.clear()
EndEvent

Function clear()
    Debug.Notification("Clearing")
    KnownArrows = new Ammo[40]
    KnownBolts = new Ammo[40]
EndFunction

; Note: For these two functions it is important not to manipulate the array returned by them. That would mess with 
; the inner working of this script. These arrays must only ever be iterated over and read from outside of this script.
Ammo[] Function GetKnownArrows()
    return KnownArrows
EndFunction

Ammo[] Function GetKnownBolts()
    return KnownBolts
EndFunction

; Checks if the given ammo type is already known and adds it to the array if needed. It looks much more complicated
; than it is. It's the exact same procedure twice, one for bolts, one for arrows.
;
; Returns true if ammo type was added to the list.
Bool Function AddIfNeeded(Ammo newAmmo, Bool allowSort = true)
    if newAmmo.isBolt()
        ; Find out if the ammo is already in the list.
        If KnownBolts.Find(newAmmo) < 0
            ; Returns the first free place in the array. Yes, the function name says the opposite, leave me alone.
            Int position = GetLastNotNoneIndex(KnownBolts)
            Debug.Trace("AEA: Adding" + newAmmo.GetName() + " at position " + position)
            KnownBolts[position] = newAmmo
            if allowSort
                ; If allowed, sort the array.
                SortArrayByDamage(KnownBolts)
            endif
            return true
        EndIf
    else
        ; The exact same as above, just for arrows instead of bolts, so I won't comment it again.
        If KnownArrows.find(newAmmo) < 0
            Int position = GetLastNotNoneIndex(KnownArrows)
            Debug.Trace("AEA: Adding" + newAmmo.GetName() + " at position " + position)
            KnownArrows[position] = newAmmo
            if allowSort
                SortArrayByDamage(KnownArrows)
            endif
            return true
        EndIf
    endif

    return false
EndFunction

; The name of this function is actually wrong. It, in fact, returns the first none index. The reason here is that I 
; slightly changed it but the function was already present so I didn't want to risk renaming it.
;
; Returns the first free spot in the array.
Int Function GetLastNotNoneIndex(Ammo[] someArray) global
    Int arrayLength = someArray.Length
    Int x = 0
    While (x < arrayLength)
        if someArray[x] == None
            return x
        endif
        x += 1
    EndWhile
    return x
EndFunction

; This function does two things. 
; First: It pushes all elements in the array to the top, so there are no random none elements inbetween.
; Second: It sorts the ammo types in the array by their damage value in descendeng order, so the most damaging ammo 
; ends up at the index 0
Function SortArrayByDamage(Ammo[] someArray) global
    Int InnerIndex
    Int OuterIndex = someArray.length - 1

    while OuterIndex > 0
        InnerIndex = 0
        while InnerIndex < OuterIndex
            ; If the element is none, push it down. This means after sorting, all none elements will habe been 
            ; moved to the bottom.
            if someArray[InnerIndex] == none
                someArray[InnerIndex] = someArray[InnerIndex + 1]
                someArray[InnerIndex + 1] = none

            ; Looks more complicated than it is. The first line of this condition simply checks if both relevant 
            ; elements are in fact no none, if one of them were, the rest of the check can't happen.
            ; The second like then compares their damage values. Overall it's simply a modified bubblesort.
            elseif someArray[InnerIndex] != none && someArray[InnerIndex + 1] != none && \
                    someArray[InnerIndex].getDamage() < someArray[InnerIndex + 1].getDamage()
                SwapArrayElements(someArray, InnerIndex, InnerIndex + 1)
            endif
            InnerIndex += 1
        EndWhile
        OuterIndex -= 1
    EndWhile
EndFunction

; Simple utitlity function to swap two array elements with the patented "a students first algorithm" method.
Function SwapArrayElements(Ammo[] someArray, Int firstIndex, Int secondIndex) global
    Ammo temp = someArray[firstIndex]
    someArray[firstIndex] = someArray[secondIndex]
    someArray[secondIndex] = temp
EndFunction


; Iterates over all items in the Players inventory, trying to find and add to the list ammo types. The sole 
; parameter should ideally contain a reference to the player but any actor with an inventory will work.
; Though there isn't much sense in looking for usable ammo in an inventory other than the players I guess.
Function ScanInventoryForAmmo(ObjectReference inRef)
    ;Debug.Trace("[Auto Equip Ammo]: Scanning inventory for ammo: " + inRef.GetName())
    Int ItemIndex = inRef.GetNumItems()
    While ItemIndex > 0
        ItemIndex -= 1
        Form Item = inRef.getNthForm(ItemIndex)
        if item.GetType() == 42
            Ammo objAmmo = item as Ammo
            ;Debug.Trace("[Auto Equip Ammo]: Found Ammo in Inventory: " + objAmmo.GetName())
            AddIfNeeded(objAmmo, false)
        endif
    EndWhile
    ;Debug.Trace("[Auto Equip Ammo]: Sorting Arrays...")
    SortArrayByDamage(KnownBolts)
    SortArrayByDamage(KnownArrows)
    ;Debug.Trace("[Auto Equip Ammo]: Done")
EndFunction