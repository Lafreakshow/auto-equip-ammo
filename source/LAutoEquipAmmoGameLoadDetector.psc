ScriptName LAutoEquipAmmoGameLoadDetector extends ReferenceAlias
; This script is only used to listen to the OnPlayerLoadGame event, which can only be done when attached to an actor.


LAutoEquipAmmoQuest property LAutoEquipAmmoManager Auto

Event OnInit()
    ; This script was added after the first version so some people may have the mod already running.
    ; So we just make sure to also check for a version change on first load.
    LAutoEquipAmmoManager.CheckForVersionChange()
EndEvent

Event OnPlayerLoadGame()
    LAutoEquipAmmoManager.CheckForVersionChange()
EndEvent