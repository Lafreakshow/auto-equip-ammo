Scriptname LafreakshowPapyrusLib extends Quest
; A collection of utility functions for my mods 

; ----------------------------------------------------------------------------------------------------------------------
; Properties

; Use these to instead of passing raw integers to avoid problems with future updates to the logging functions
Int property LEVEL_ALWAYS  = 0 autoreadonly
Int property LEVEL_ERROR   = 1 autoreadonly
Int property LEVEL_WARNING = 2 autoreadonly
Int property LEVEL_INFO    = 3 autoreadonly

; ----------------------------------------------------------------------------------------------------------------------
; For internal use

Int _debugLevel = 0
string _debugMessagePrefix = ""

; ----------------------------------------------------------------------------------------------------------------------
; Logging and notification related functions


; Set the highest message level that should be displayed. I reccomend to use the LEVEL_* properties but any positive 
; Integer will work if the predefined level don't satify your needs.
Function SetDebugLevel(Int level)
    _debugLevel = level
EndFunction


; Return the current highest message level that will be displayed.
Int Function GetDebugLevel()
    return _debugLevel
EndFunction


; Set a prefix for all messages sent through this library. Defaults to "".
Function SetDebugMessagePrefix(String prefix)
    _debugMessagePrefix = prefix
EndFunction


; Try to send a notification with the specified level to the player. The message will only be sent if the given level 
; is lower than the level set with SetDebugLevel() (defaults to LEVEL_ALWAYS).
;
; Will by default also write the message to the papyrus log, to disable this set alsoWriteToLog to false.
Function DebugNotification(String msg, int Level = 0, Bool alsoWriteToLog = true)
    if level <= _debugLevel
        String finalMsg = _debugMessagePrefix + msg

        Debug.notification(finalMsg)

        if alsoWriteToLog
            Debug.Trace(finalMsg)
        endif
    endif
EndFunction


; Return the name of the property associated to an integer log leve. Returns "UNKOWN LEVEL!" if there is no associated 
; property
String Function GetLevelAsString(int level)
    if level == LEVEL_ALWAYS
        return "LEVEL_ALWAYS"
    elseif level == LEVEL_ERROR
        return "LEVEL_ERROR"
    elseif level == LEVEL_WARNING
        return "LEVEL_WARNING"
    elseif level == LEVEL_INFO
        return "LEVEL_INFO"
    else 
        return "UNKNOWN LEVEL!"
    endif
EndFunction


; Get the integer level associated with the name of property. Will return -1 if there is no associated property.
Int Function GetLevelFromString(String level)
    if level == "LEVEL_ALWAYS"
        return LEVEL_ALWAYS
    elseif level == "LEVEL_ERROR"
        return LEVEL_ERROR
    elseif level == "LEVEL_WARNING"
        return LEVEL_WARNING
    elseif level == "LEVEL_INFO"
        return LEVEL_INFO
    else 
        return -1
    endif
EndFunction