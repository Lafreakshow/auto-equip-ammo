ScriptName LAutoEquipAmmoQuest Extends Quest
; This script manages the Ammo Change ability and acts as a sort of hub for everything to come together.


; ----------------------------------------------------------------------------------------------------------------------
; Properties

Quest property LAutoEquipAmmoManagingQuest Auto
Spell property LAutoEquipAmmoAbility Auto
Actor property PlayerRef Auto

LAutoEquipKnownAmmoList property KnownAmmo auto
LafreakshowPapyrusLib   property util auto

; MCM Settings
Bool   property IsChangeInCombatAllowed   = true auto
Bool   property UseHotKeys                = false auto
Bool   property ChangeOnWeaponEquip       = true auto
Int    property EnableToggleHotkey        = 49 auto
Float  property OutOfCombarUpdateInterval = 5.0 auto

; Using custom get and set functions here makes it possible to remove or add the ability to the player by 
; simply doing: IsModEnabled = true/false.
;
; Func Fact: Coming from Java/Kotlin, this is one of the very few things in Papyrus that feel comfortable to use to me.
Bool _isModEnabled = true
Bool Property IsModEnabled
    Function Set(Bool newValue)
        _isModEnabled = newValue
        If _isModEnabled
            PlayerRef.AddSpell(LAutoEquipAmmoAbility)
        Else 
            PlayerRef.RemoveSpell(LAutoEquipAmmoAbility)
        EndIf
    EndFunction

    Bool Function Get()
        return _isModEnabled
    EndFunction
EndProperty


; ----------------------------------------------------------------------------------------------------------------------
; For internal use

Bool isInitialized   = false
Int RunningVersion = 0


; ----------------------------------------------------------------------------------------------------------------------
; Events

Event OnInit()
    if isInitialized
        Init()
    endif
EndEvent

; This event takes care of all the hotkey handling.
Event OnKeyDown(int keyCode)
    if keyCode == EnableToggleHotkey
        ; Note that this if statement is pretty much only here for the notifications.
        ; There is no problem doing IsModEnabled = !IsModEnabled like I did in many other places.
        ; In fact, that's how it used to be but then I wanted to add some feedback for the player.
        if IsModEnabled
            util.DebugNotification("Mod disabled", util.LEVEL_ALWAYS)
            IsModEnabled = false
        else
            util.DebugNotification("Mod Enabled", util.LEVEL_ALWAYS)
            IsModEnabled = true
        endif       
    endif
EndEvent


; ----------------------------------------------------------------------------------------------------------------------
; Functions

Function Init()
    Debug.Notification("Auto Equip Ammo: Started!")
    _isModEnabled = true
    isInitialized = true
    KnownAmmo.ScanInventoryForAmmo(PlayerRef)
    SetUpLogging()
EndFunction

; This function exists so it is easier to maintain and more consistent to change logging related 
; stuff from other scripts. It only does one thing now but that can easily be expanded without having to edit a
; bunch of other functions. I like me some future proofing now and then.
Function SetUpLogging()
    util.SetDebugMessagePrefix("[AutoEquipAmmo] ")
EndFunction

Function UpdateRegisteredKeys()
    util.DebugNotification("Updating Hotkey Registration", util.LEVEL_INFO)
    if UseHotKeys
        RegisterForKey(EnableToggleHotkey)
    else
        ; If hotkeys are disabled completely, we don't need to listen for events at all, so just unregister everything.
        UnregisterForAllKeys()
    endif
EndFunction

; This function is used to reset everything to the default status it should have straight after installing.
; The main reason we need this is script updates in the future, which could otherwise lead to undefined behaviour due 
; to variables not being initialized properly.
Function ResetToDefault()
    IsModEnabled            = true
    EnableToggleHotkey      = 49
    IsChangeInCombatAllowed = true
    UseHotKeys              = false
    util.SetDebugLevel(util.LEVEL_ALWAYS)
    KnownAmmo.Clear()
    KnownAmmo.ScanInventoryForAmmo(PlayerRef)
EndFunction

; It's important to have the value hardcoded here. The reason is when the mod is updated on an existing save, all 
; exisitng variables will be loaded from that save whereas hardcoded values in functions will be properly taken from 
; the updated script file. The same also goes for the version checking mechanism in the MCM. 
; (In fact, the MCM documentation is where I got this idea from.)
Int Function GetVersion()
    return 1
EndFunction

; In this function we perform all the necessary work to get the script up to the current version. If the mod is newly 
; installed, none of this needs to run.
Function CheckForVersionChange()
    if GetVersion() > RunningVersion
        Debug.Notification("Auto Equip Ammo: Detected Script Version Change")
        if RunningVersion < 1
            Debug.Trace("Auto Equip Ammo: Updating to Script version 0.2")
            IsModEnabled = false
            ResetToDefault()
            SetUpLogging()
        endif
        RunningVersion = GetVersion()
    endif
EndFunction

; This function pretty much kills the mod. Removes the ability and stops the quest this script is attached to, 
; preventing it from running again until the quest is restarted.
Function UninstallMod() 
    PlayerRef.RemoveSpell(LAutoEquipAmmoAbility)
    UnregisterForAllKeys()
    Self.Stop()
EndFunction