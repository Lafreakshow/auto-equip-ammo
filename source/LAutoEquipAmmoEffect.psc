ScriptName LAutoEquipAmmoEffect Extends ActiveMagicEffect
; This script contains all the ammo switching magic


; ----------------------------------------------------------------------------------------------------------------------
; Properties


Actor                   property playerRef auto
LAutoEquipAmmoQuest     property ManagerQuest auto
LAutoEquipKnownAmmoList property KnownAmmo auto
LafreakshowPapyrusLib   property util auto


; ----------------------------------------------------------------------------------------------------------------------
; Internal use Variables


Ammo lastUsedArrowType
Ammo lastUsedBoltType
Bool checkAfterCombat = false


; ----------------------------------------------------------------------------------------------------------------------
; Events


; In order to receive animation events we have to tell the game which animations we want to know about
; This is done as soon as the effect is applied to the player
Event OnEffectStart(Actor akTarget, Actor akCaster)
	RegisterForAnimationEvent(playerRef, "BowRelease")
EndEvent


; I believe Papyrus cleans up listeners automatically when the effect ends but it is good practice for a program to 
; clean up after itself.
Event OnEffectFinish(Actor akTarget, Actor akCaster)
  UnregisterForAnimationEvent(playerRef, "BowRelease")
EndEvent


; The script needs to learn new ammo types and the only way to do that without hard coding or an SKSE plugin is to check
; every item the player picks up.
Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	util.DebugNotification("Picked Up " + akBaseItem.GetName(), util.LEVEL_INFO)

	; If the item is of type Ammo, we ask the KnownAmmoList to check if it is already known or add it to the 
	; list if it is not.
	if (akBaseItem as Ammo)
		ManagerQuest.KnownAmmo.AddIfNeeded(akBaseItem as Ammo)
	endif
EndEvent


; This function is called whenever the player Equips something.
; We need to know when the player equips a ranged weapon or ammo.
Event OnObjectEquipped(Form akBaseObject, ObjectReference akReference)
	util.DebugNotification("Equipped " + akBaseObject.GetName(), util.LEVEL_INFO)

	; A failed cast counts as false
	if akBaseObject as Ammo
		Ammo akAmmoObject = akBaseObject as Ammo

		; We use this handy SKSE function to determine whether the ammo that was just equipt are bolts or arrows.
		; This point here is why SKSE is a hard dependency for this mod. Without it, there seems to be know way to 
		; differentiate bolts and arrows without relying on their name.
		if akAmmoObject.IsBolt()
			lastUsedBoltType = akAmmoObject
		Else
			lastUsedArrowType = akAmmoObject
		endif

	; If it was not ammo, we do a second check to see if it was a weapon.
	; Whenever you equip a ranged weapon and don't have ammo already equipt, the game autoselects the weakest ammo. 
	; We don't like that so when a bow or crossbow is equipt, we make sure that the best ammo is selected.
	ElseIf ManagerQuest.ChangeOnWeaponEquip && akBaseObject as Weapon
		Weapon akWeaponObject = akBaseObject as Weapon
		; 7 means bow, obviously
		if akWeaponObject.GetWeaponType() == 7
			EquipBestAmmoType(playerRef, false)
		; 9 means crossbow as any child knows
		ElseIf akWeaponObject.GetWeaponType() == 9
			EquipBestAmmoType(playerRef, true)
		EndIf
	endIf

EndEvent


; There exists an event called OnPlayerBowShot(). Sounds like it is exactly what we need right? 
; Except for some reason it doesn't work with crossbows... So instead we listen for the BowRelease 
; animation which *does* apply to crossbows. Totally makes sense.
; Anyway, all this is here to change the ammo when the player runs out.
Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	if asEventName == "BowRelease"

		util.DebugNotification("Shot an arrow", util.LEVEL_INFO)
		; First we once again check which weapon we are currently holding and then we check for the corresponding ammo.
		; We only want to switch ammo here if the last used ammo is empty. This saves a heckin lot of processing time 
		; over my prevous method of changing the ammo whenever an arrow was fired...
		if playerRef.GetEquippedWeapon().GetWeaponType() == 7 && playerRef.GetItemCount(lastUsedArrowType as Ammo) <= 1
  
			util.DebugNotification("Shot last arrow", util.LEVEL_INFO)
			EquipBestAmmoType(playerRef, false)

		; Once again, 7 means bow, 9 means crossbow
		ElseIf playerRef.GetEquippedWeapon().GetWeaponType() == 9 && playerRef.GetItemCount(lastUsedBoltType as Ammo) <= 1
			
			util.DebugNotification("Shot last bolt", util.LEVEL_INFO)
			EquipBestAmmoType(playerRef, true)

		endif
	EndIf
EndEvent


; This event is only fired when the script registers for it. 
; The game doesn't probide an event that fires when the player enter/leaves combat. So if we want to delay an ammo 
; change until after combat we need to check in a regular interval if combat has ended.
Event OnUpdate()
	if playerRef.IsInCombat()
		; If the player is still in combat we register for another update and don't do anything more.
		RegisterForSingleUpdate(ManagerQuest.OutOfCombarUpdateInterval)
	else
		; If the play is out of combat we do the same old ammo changing procedure.
		if playerRef.GetEquippedWeapon().GetWeaponType() == 7 
			EquipBestAmmoType(playerRef, false)
		ElseIf playerRef.GetEquippedWeapon().GetWeaponType() == 9
			EquipBestAmmoType(playerRef, true)
		endif
		; Lastly we have to reset this variable.
		checkAfterCombat = false
	endif
EndEvent


; ----------------------------------------------------------------------------------------------------------------------
; Functions


; This function takes the best Ammo available in the player Inventory and equipts it. 
; The first parameter is the actor on which to apply the function and the second tells us 
; whether we are looking for bolts or arrows
Function EquipBestAmmoType(actor inActor, Bool isBolt)
	if playerRef.IsInCombat() && !ManagerQuest.IsChangeInCombatAllowed
		; If changes in while in combat are prohibited we register for an update so the script can check back after 
		; combat has ended.
		if !checkAfterCombat
			RegisterForSingleUpdate(ManagerQuest.OutOfCombarUpdateInterval)
		endif
		; this prevents the script fdrom registering for multiple updates at the same time and must be reset when combat
		; has ended.
		checkAfterCombat = true
		return
	endif

	Ammo newAmmo = SelectBestAmmoType(playerRef, isBolt)
	; newAmmo will only be none if no usable ammo is in the players inventory. In any other case, the KnownAmmoList 
	; probably needs to be updated
	if newAmmo != None && IsChangeNeeded(newAmmo, isBolt)
		; We set the last parameter to true here to avoid the standard Skyrim "X Equipped" message. 
		; We don't need it since we have our own message, which is  important because it shows 
		; the user that the mod actually does stuff
		playerRef.EquipItem(newAmmo, false, true)
		util.DebugNotification("Switched to " + newAmmo.GetName())
	EndIf
EndFunction

; This is where the magic happens. 
; Parameters here are exactly the same as for EquipBestAmmoType().
Ammo Function SelectBestAmmoType(Actor inActor, Bool isBolt)
	; First we get the list of either Arrows or Bolts, depending on which type of ammo is needed.
	Ammo[] AmmoList
	if isBolt
		AmmoList = KnownAmmo.GetKnownBolts()
	else
		AmmoList = KnownAmmo.GetKnownArrows()
	endif
	
	; We interate over the list of known Arrows/Bolts and test for each if it is in the players inventory, 
	; if it is, it will be returned.
	; Previously the script would iterate over the players inventory and compare all ammo to each other but the new 
	; method is much kinder on performance while still being compatible with any mod that adds new ammo.
	Int Index = 0
	While (index < AmmoList.Length)
		If playerRef.GetItemCount(AmmoList[Index]) > 0
			return AmmoList[Index]
		endif
		Index += 1
	EndWhile

	Return None
endfunction

; Make sure a change is actually needed. We don't want to change ammo if we are already using the best.
Bool Function IsChangeNeeded(Ammo newAmmo, Bool isBolt)
	if isBolt
		; Return true if new ammo is not the same as the last used bolt
		If newAmmo != lastUsedBoltType
			return true			
		EndIf
	Else
		; Return true if new ammo is not the same as the last used arrow
		if newAmmo != lastUsedArrowType 
			return true 
		EndIf
	EndIf
	; Return false if any condition fails. This should only happen if the new ammo type is already equipped
	return False
EndFunction

