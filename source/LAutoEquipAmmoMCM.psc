ScriptName LAutoEquipAmmoMCM Extends SKI_ConfigBase
; This script controls everything related to the MCM.


; ----------------------------------------------------------------------------------------------------------------------
; Properties

Actor                   property playerRef auto
LAutoEquipAmmoQuest     property LAutoEquipAmmoManager Auto
LAutoEquipKnownAmmoList property KnownAmmoList auto
Spell                   property LAutoEquipAmmoAbility Auto
LafreakshowPapyrusLib   property util auto

; ----------------------------------------------------------------------------------------------------------------------
; Version Control

int Function GetVersion()
	; Note, This is independent from the overall mod version.
	return 2
EndFunction

Event OnVersionUpdate(int version)
	Debug.Notification("Auto Equip Ammo: Detected MCM Version Change: " + version + ">>" + GetVersion())
	if version < 2
		UpdateMod()
	endif
EndEvent

; ----------------------------------------------------------------------------------------------------------------------
; MCM Menu

event OnConfigInit()
	Pages = new string[2]
	Pages[0] = "General"
	Pages[1] = "Registered Ammo"
endEvent

Function DisplayGeneralPage()
	SetCursorFillMode(TOP_TO_BOTTOM)
	SetCursorPosition(0)
	AddHeaderOption("General Options")
	AddToggleOptionST("EnableToggleOptionST", "Enable Mod", LAutoEquipAmmoManager.IsModEnabled)
	AddToggleOptionST("CombatRestrictionOptionST", "Allow change in Combat", LAutoEquipAmmoManager.IsChangeInCombatAllowed)
	AddToggleOptionST("ChangeOnWeaponEquipST", "Change on Weapon Equip", LAutoEquipAmmoManager.ChangeOnWeaponEquip)

	AddHeaderOption("Hotkeys")
	AddToggleOptionST("UseHotKeysToggleST", "Use Hotkeys", LAutoEquipAmmoManager.UseHotKeys)
	Int hkflags = getHotkeyOptionFlags(LAutoEquipAmmoManager.UseHotKeys)
	AddKeyMapOptionST("ModToggleKeyOptionST", "Enable/Disable Toggle", LAutoEquipAmmoManager.EnableToggleHotkey , hkflags)

	SetCursorPosition(1)
	AddHeaderOption("Maintenance")
	AddToggleOptionST("UpdateToggleOptionST", "Reset and update Mod", false)
	AddToggleOptionST("UninstallToggleST", "Uninstall", false)
	AddMenuOptionST("EnableDebugMessagesST", "Message Level", util.getLevelAsString(util.getDebugLevel()))

	AddHeaderOption("Debug Information")
	AddTextOption("Player has Spell", playerRef.HasSpell(LAutoEquipAmmoAbility))
	AddTextOption("Manager Quest Running", LAutoEquipAmmoManager.IsRunning())
	AddTextOption("MCM Quest Running", Self.IsRunning())
	AddTextOption("MCM Version", GetVersion())
	AddTextOption("Script Version", LAutoEquipAmmoManager.GetVersion())
EndFunction

Function DisplayRegisteredAmmoPage()
	SetCursorFillMode(TOP_TO_BOTTOM)
	SetCursorPosition(0)
	AddToggleOptionST("ScanInventoryForAmmoToggleST", "Scan Inventory", false)
	AddHeaderOption("Arrows")

	; Iterate over known arrows to add a control for each of them dynamically. 
	; The same also happens for Bolts further down
	Ammo[] Arrows = KnownAmmoList.GetKnownArrows()
	int ArrayLength = Arrows.length
	int x = 0
	While (x < ArrayLength)
		if Arrows[x] != None
			AddTextOption(x + ": " + Arrows[x].GetName(), Arrows[x].GetDamage() as Int)
		endif
		x += 1
	EndWhile

	SetCursorPosition(1)
	AddEmptyOption()
	AddHeaderOption("Bolts")

	Ammo[] Bolts = KnownAmmoList.GetKnownBolts()
	ArrayLength = Bolts.length
	x = 0
	While (x < ArrayLength)
		if Bolts[x] != None
			AddTextOption(x + ": " + Bolts[x].GetName(), Bolts[x].GetDamage() as Int)
		endif
		x += 1
	EndWhile
EndFunction

Event OnPageReset(String page)
	if page == "General"
		DisplayGeneralPage()
	elseif Page == "Registered Ammo"
		DisplayRegisteredAmmoPage()
	endif
EndEvent

; ----------------------------------------------------------------------------------------------------------------------
; Menu controls

State ScanInventoryForAmmoToggleST
	Event OnSelectST() 
		SetOptionFlagsST(OPTION_FLAG_DISABLED)
		KnownAmmoList.ScanInventoryForAmmo(playerRef)
		; Forcing page reset here so the list of known arrows is updated without the menu having to be closed.
		; There seems to be an issue where the function above doesn't return until the menu closes and I don't know why,
		; so this may actually be useless.
		ForcePageReset()
	EndEvent

	Event OnHighlightST()
		SetInfoText("Scan the players inventory for ammo not yet known to the script." + \
		"Use if you have any ammo in your invenotry that doesn't show up in the list below.")
	EndEvent
EndState

State UseHotKeysToggleST
	Event OnSelectST()
		LAutoEquipAmmoManager.UseHotKeys = !LAutoEquipAmmoManager.UseHotKeys
		SetToggleOptionValueST(LAutoEquipAmmoManager.UseHotKeys)
		setHotkeyOptionFlags(LAutoEquipAmmoManager.UseHotKeys)
		LAutoEquipAmmoManager.UpdateRegisteredKeys()
	EndEvent

	Event OnHighlightST()
		SetInfoText("Completely disable hotkeys")
	EndEvent
EndState

State ModToggleKeyOptionST
	Event OnKeyMapChangeST(int a_keyCode, string a_conflictControl, string a_conflictName)
		; We are checking here if the key chosen by the player is already bound to something and if it is, we ask for 
		; confirmation.
		if a_conflictControl != ""
			String ConflictMessage = "Key already bound to " + a_conflictControl 
			if a_conflictName != ""
				ConflictMessage += " " + a_conflictName
			EndIf
			ConflictMessage += "\n\n Do you want to change it anyway?"
			Bool playerAccepted = ShowMessage(ConflictMessage)
			if playerAccepted
				ChangeEnableHotkey(a_keyCode)
				SetKeyMapOptionValueST(a_keyCode)
			EndIf
		else
			ChangeEnableHotkey(a_keyCode)
			SetKeyMapOptionValueST(a_keyCode)
		endif
	EndEvent

	Event OnHighlightST()
		SetInfoText("Change the key used to toggle the mod on or off")
	EndEvent
EndState

State CombatRestrictionOptionST
	Event OnSelectST()
		LAutoEquipAmmoManager.IsChangeInCombatAllowed = !LAutoEquipAmmoManager.IsChangeInCombatAllowed
		SetToggleOptionValueST(LAutoEquipAmmoManager.IsChangeInCombatAllowed)
	EndEvent

	Event OnHighlightST()
		SetInfoText("When disabled, ammo will only be changed out of combat.")
	EndEvent
EndState

State EnableToggleOptionST
	Event OnSelectST()
		LAutoEquipAmmoManager.IsModEnabled = !LAutoEquipAmmoManager.IsModEnabled
		SetToggleOptionValueST(LAutoEquipAmmoManager.IsModEnabled)
		; Without a page reset here the debug information shown would only update after the menu ist closed.
		; It's not at all important for functionality, mostly here to make debugging easier.
		ForcePageReset()
	EndEvent

	Event OnHighlightST()
		SetInfoText("Toggle the mod on or off. Has the same effect as using the off toggle hotkey.")
	EndEvent
EndState

State ChangeOnWeaponEquipST
	Event OnSelectST()
		LAutoEquipAmmoManager.ChangeOnWeaponEquip = !LAutoEquipAmmoManager.ChangeOnWeaponEquip
		SetToggleOptionValueST(LAutoEquipAmmoManager.ChangeOnWeaponEquip)
	EndEvent

	Event OnHighlightST()
		SetInfoText("When enbabled, the mod will check if a an ammo change is necessry whenever a ranged weapon is equipped")
	EndEvent
EndState

State UpdateToggleOptionST
	Event OnSelectST()
		UpdateMod()
		Debug.MessageBox("Mod Reset")
	EndEvent

	Event OnHighlightST()
		SetInfoText("Reset Mod to Default values and check for version change.")
	EndEvent
EndState

State UninstallToggleST
	Event OnSelectST()
		; Since the mod pretty much has to be reinstalled after this uninstall option was used, we want to make 
		; REALLY sure it is what the placer actually wants. 
		Bool REALLYREALLYSURE = ShowMessage("Are you sure you want to uninstall the mod?", true, \
											"Uninstall", "Don't Uinstall!!!")
		if REALLYREALLYSURE
			LAutoEquipAmmoManager.UninstallMod()
			Self.Stop()
		endif
	EndEvent

	Event OnHighlightST()
		SetInfoText("Stop all related quests and scripts so that the mod is safe to remove." + \
		"ONLY USE THIS IF YOU WANT TO UPDATE OR REMOVE THE MOD")
	EndEvent
EndState

State EnableDebugMessagesST
	Event OnMenuOpenST()
		; Displaying all the possible option in the popup selection menue
		String[] options = new String[4]
		options[0] = util.getLevelAsString(0)
		options[1] = util.getLevelAsString(1)
		options[2] = util.getLevelAsString(2)
		options[3] = util.getLevelAsString(3)

		SetMenuDialogOptions(options)
	EndEvent

	Event OnMenuAcceptST(int a_index)
		util.SetDebugLevel(a_index)
		SetMenuOptionValueST(util.getLevelAsString(a_index))
	EndEvent

	Event OnHighlightST()
		SetInfoText("You probably don't need to touch this ever")
	EndEVent
EndState

; ----------------------------------------------------------------------------------------------------------------------
; Various Functions

Function UpdateMod()
	LAutoEquipAmmoManager.ResetToDefault()
	LAutoEquipAmmoManager.CheckForVersionChange()
EndFunction

Function ChangeEnableHotkey(int newKey)
	LAutoEquipAmmoManager.EnableToggleHotkey = newKey
	LAutoEquipAmmoManager.UpdateRegisteredKeys()
EndFunction

; If hotkeys are disabled, this function will return the DISABLED flag, so that all hotkey related controls can 
; be disabled in a convenient way. Otherwise it returns the UNMAP flag which allows key slection options to be cleared 
; of any selection, instead of just being reset to default.
Int Function getHotkeyOptionFlags(Bool HotkeysEnabled)
	Int HotKeyFlags = OPTION_FLAG_WITH_UNMAP
	if !HotKeysEnabled
		HotKeyFlags = OPTION_FLAG_DISABLED
	EndIf
	return HotKeyFlags
EndFunction

; Right now there is only one hotkey option so it would be easy enough to do this in place but I want to add more hotkeys
; in future versions and making sure all of them get the correct flags all the time would be a pain in the ass without 
; collecting all these calls in one place. 
; Bascially this function just gives the correct flags to all hotkey options.
Function setHotkeyOptionFlags(Bool HotkeysEnabled)
	Int HotKeyFlags = getHotKeyOptionFlags(HotkeysEnabled)

	SetOptionFlagsST(HotKeyFlags, false, "ModToggleKeyOptionST")
EndFunction