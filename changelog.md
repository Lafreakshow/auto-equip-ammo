# 0.2.0 - A big one

+ Ammo change logic rewritten. Now uses a list of known ammo instead of searching the player inventory every time. 
+ Previously unknown ammo will be registered when picked up. 
+ MCM menu added
    + General page
        + Toggle the mod on or off
        + Restrict ammo switching to out of combat
        + Prevent ammo switching on weapon equip
        + Toggle all hotkeys on/off
        + Set On/Off toggle hotkey
        + Reset and update option. Resets the mod to default and forces and update check
        + Uninstall option. Stops all quests and removes the magic effect so the mod can be safely removed.
        + Some Debug information (If you encounter issues, it would be a good idea to include this in your bug report)
    + Registered Ammo Page
        + Scan inventory option. Forces the script to search the players inventory for ammo. 
        + List of known Arrows and bolts, sorted by damage. 
+ Added infrastructure to detect and deal with version changes. Should make future updates a little smoother.
+ Mod now comes with BSA.

# 0.1.0 - First Release