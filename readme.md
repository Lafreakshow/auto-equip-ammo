# Lafreakshow's Auto Equip Ammo for Skyrim SE
The serious lack of functioning and compatible mods that equip the best, rather than the vanilla default of worst, ammo prompted me to make one my of own. Here it is.

* Works for Crossbows and Bows.
* Compatible with all mods that add new arrows or bolts
* Highly configurable via MCM

Note that this mod does a little more than just change which ammo is equipped when you run out. If that's all you're after, you may want to look for a more lightweight and simple alternative. 

Unfortunately I don't have a recommendation right now. If you find one that works, tell me so I can recommend it here.

# Future plans
Eventually I hope to build a blacklist/priority feature so users can customize which ammo will be equipped first. 

I also want to add hotkeys that allow for cycling ammo in the players inventory.

# Installation + Load Order
Install with your favorite mod manager or manually drop into the data directory. Load order doesn't matter, the ESP is ESL flagged.
