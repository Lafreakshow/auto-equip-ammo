#!/usr/bin/env python3
"""
A little script I wrote to make creating archives for release on Nexus easier. It will 
read in a json file named "made_dist_archive.jso" of the following pattern:
{
    "NAME_OF_ARCHIVE": {
        "DIRECTORY_WITHIN_ARCHIVE": [
            "PATH_TO_FILE_TO_INCLUDE_IN_THAT_DIRECTORY"
        ]
    }
}

Note: In order to get the final, BSA making version running I spent many hours studying how 
Chesko builds Frostfall. He uses a similar script. The idea of copying files to temporary 
locations is borrowed from that script.

"""

import json, shutil, os, subprocess, argparse
from zipfile import ZipFile
from pathlib import Path

# Ini file variables
TEMPORARY_FOLDER           : str
OUTPUT_FOLDER              : str
ARCHIVE_EXE_LOCATION       : str
FILES_TO_COPY_FOR_BSA      : dict
BSA_THINGS_TO_CHECK        : list
BSA_NAME                   : str
DIST_ARCHIVE_NAME          : str
FILES_TO_INCLUDE_IN_ARCHIVE: list
FILES_TO_CLEAN_UP          : list

# CMD argument variables
MAKE_NEXUS_ARCHIVE = True
MAKE_BSA_ARCHIVE = True

# Constants
ARCHIVE_SCRIPT_PREFIX = "[Make Archive}"
ARGPARSE_DESCRIPTION  = "Makes BSA archive and zip archive for nexus release"
def prepare_temporary_folder():
    print(ARCHIVE_SCRIPT_PREFIX, "Preparing...")
    
    try:
        if Path(TEMPORARY_FOLDER).exists():
            print(ARCHIVE_SCRIPT_PREFIX, "Temp dir already exists, cleaning up...")
            shutil.rmtree(TEMPORARY_FOLDER)

        print(ARCHIVE_SCRIPT_PREFIX, "Making Temp directory:", TEMPORARY_FOLDER)
        os.makedirs(TEMPORARY_FOLDER)

        print(ARCHIVE_SCRIPT_PREFIX, "Copying Archive.exe...")
        shutil.copyfile(ARCHIVE_EXE_LOCATION, Path(TEMPORARY_FOLDER).joinpath("Archive.exe"))

        with open(Path(TEMPORARY_FOLDER).joinpath("files_for_bsa.txt"), "w", newline="\r\n") as bsa_file:
            for folder, files in FILES_TO_COPY_FOR_BSA.items():
                target = Path(folder)
                os.makedirs(Path(TEMPORARY_FOLDER, "data").joinpath(target))
                print(ARCHIVE_SCRIPT_PREFIX, "Copying files to", target)
                for fileVar in files:
                    destPath = Path(target).joinpath(Path(fileVar).name)
                    fullDestPath = Path(TEMPORARY_FOLDER, "data").joinpath(destPath)

                    shutil.copyfile(Path(fileVar), fullDestPath)
                    bsa_file.write(str(destPath) + "\n")
                    print(ARCHIVE_SCRIPT_PREFIX,"\t", fileVar)    

    except FileNotFoundError as notfound:
        print(ARCHIVE_SCRIPT_PREFIX, "A file wasn't found:", notfound.filename)
    
    if not Path(OUTPUT_FOLDER).exists():
        print(ARCHIVE_SCRIPT_PREFIX, "Making output directory:", OUTPUT_FOLDER)
        os.makedirs(OUTPUT_FOLDER)
    else:
        print(ARCHIVE_SCRIPT_PREFIX, "Output directory already exists:", OUTPUT_FOLDER)


def write_bsa_script():
    print(ARCHIVE_SCRIPT_PREFIX, "Writing Archive.exe script...")
    with open(Path(TEMPORARY_FOLDER).joinpath("bsa_script.txt"), "w", newline="\r\n") as bsa_script:
        bsa_script.write("Log: BSA_Archive_log.txt\n")
        bsa_script.write("New Archive\n")

        for check in BSA_THINGS_TO_CHECK:
            bsa_script.write("Check: " + check + "\n")

        bsa_script.write("Set File Group Root: Data\\\n")
        bsa_script.write("Add File Group: files_for_bsa.txt\n")
        bsa_script.write("Save Archive: ../" + str(Path(BSA_NAME)))


def make_bsa():
    cur_cwd = os.getcwd()
    os.chdir(TEMPORARY_FOLDER)
    print(ARCHIVE_SCRIPT_PREFIX, "Running Archive.exe...")
    code = subprocess.call(['./Archive.exe', './bsa_script.txt'])
    print(ARCHIVE_SCRIPT_PREFIX, "Archive.exe returns:", code)

    os.chdir(cur_cwd)

def make_archive():
    mod_version = input(ARCHIVE_SCRIPT_PREFIX + " Release Version: ")
    print(ARCHIVE_SCRIPT_PREFIX, "Making archive for nexus...")
    mod_version_string = "_" + mod_version.replace(".", "_")
    archive_path = Path(OUTPUT_FOLDER, DIST_ARCHIVE_NAME + mod_version_string + ".zip")

    with ZipFile(archive_path, "w") as zipfile:
        for filename in FILES_TO_INCLUDE_IN_ARCHIVE:
            zipfile.write(filename)


def cleanup():
    for filename in FILES_TO_CLEAN_UP:
        filepath = Path(filename)
        if filepath.exists():
            if filepath.is_dir(): 
                shutil.rmtree(Path(filename))
            else:
                os.remove(filepath)


def load_config():
    global TEMPORARY_FOLDER, OUTPUT_FOLDER, ARCHIVE_EXE_LOCATION, FILES_TO_COPY_FOR_BSA
    global BSA_NAME, BSA_THINGS_TO_CHECK, DIST_ARCHIVE_NAME, FILES_TO_INCLUDE_IN_ARCHIVE
    global FILES_TO_CLEAN_UP

    with open("make_dist_archive.json") as json_file:
        config: dict = json.load(json_file)

        TEMPORARY_FOLDER            = config["temporary_folder"]
        OUTPUT_FOLDER               = config["output_folder"]
        ARCHIVE_EXE_LOCATION        = config["archive_exe_location"]
        FILES_TO_COPY_FOR_BSA       = config["files_to_copy_for_bsa"]
        BSA_THINGS_TO_CHECK         = config["bsa_script_options"]["things_to_check"]
        BSA_NAME                    = config["bsa_script_options"]["bsa_name"]
        DIST_ARCHIVE_NAME           = config["dist_archive_name"]
        FILES_TO_INCLUDE_IN_ARCHIVE = config["files_to_include_in_archive"]
        FILES_TO_CLEAN_UP           = config["files_to_clean_up"]


def build_dist_zip():
    load_config()
    if MAKE_BSA_ARCHIVE:
        prepare_temporary_folder()
        write_bsa_script()
        make_bsa()

    if MAKE_NEXUS_ARCHIVE:
        make_archive()
    
    cleanup()

def parse_args():
    global MAKE_NEXUS_ARCHIVE, MAKE_BSA_ARCHIVE

    parser = argparse.ArgumentParser(description=ARGPARSE_DESCRIPTION)
    parser.add_argument("--no_bsa", action="store_false")
    parser.add_argument("--no_zip", action="store_false")

    args = parser.parse_args()

    MAKE_BSA_ARCHIVE   = args.no_bsa
    MAKE_NEXUS_ARCHIVE = args.no_zip


if __name__ == "__main__":
    parse_args()
    build_dist_zip()